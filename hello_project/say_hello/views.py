from django.shortcuts import render

# Create your views here.
def say_hello(request):
    name = "K U"
    context = {
        "name": name,
    }
    return render(request, 'say_hello/say_hello.html', context)