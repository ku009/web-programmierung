from django.apps import AppConfig


class EinkaufszettelAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'einkaufszettel_app'
