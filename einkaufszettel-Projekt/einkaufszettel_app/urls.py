"""einkaufszettel URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path("einkaufsliste_details/<int:einkaufsliste_id>/", views.einkaufsliste_details, name="einkaufsliste_details"),
    path("add_einkaufsliste/", views.add_einkaufsliste, name="add_einkaufsliste"),
    path("delete_einkaufsliste/<int:einkaufsliste_id>/", views.delete_einkaufsliste, name="delete_einkaufsliste"),
    path("laeden/", views.laeden, name="laeden"),
    path("delete_laden/<int:laden_id>/", views.delete_laden, name="delete_laden"),
    path("add_laden", views.add_laden, name="add_laden"),
    path("add_artikel/<int:einkaufsliste_id>/", views.add_artikel, name="add_artikel"),
    path("delete_artikel/<int:artikel_id>/<int:einkaufsliste_id>/", views.delete_artikel, name="delete_artikel"),
    path("change_status_true/<int:artikel_id>/<int:einkaufsliste_id>/", views.change_status_true, name="change_status_true"),
    path("change_status_false/<int:artikel_id>/<int:einkaufsliste_id>/", views.change_status_false, name="change_status_false"),
    path("erledigte_artikel/", views.erledigte_artikel, name="erledigte_artikel"),

]
