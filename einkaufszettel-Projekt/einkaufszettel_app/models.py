from django.db import models

# Create your models here.

class Einkaufsliste(models.Model):
    name = models.CharField(max_length=200, blank=False)
    summe = models.FloatField(max_length=200, blank=False, default=0.0)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

class Laden(models.Model):
    name = models.CharField(max_length=200, blank=False)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

class Artikel(models.Model):
    status = models.BooleanField()
    name = models.CharField(max_length=200, blank=False)
    anzahl = models.IntegerField(blank=False, default=1)
    einheit_auswahl = [
        ("g", "g"),
        ("kg", "kg"),
        ("l", "Liter"),
        ("Stk", "Stück")
    ]
    # Länge muss auf 3, weil Stk 3 Zeichen hat
    einheit = models.CharField(max_length=3, choices=einheit_auswahl, default="Stk")
    preis = models.FloatField(max_length=200, blank=False, default=0.0)
    einkaufsliste = models.ForeignKey(to=Einkaufsliste, on_delete=models.CASCADE)
    laden = models.ForeignKey(to=Laden, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name