from django.contrib import admin
from .models import Einkaufsliste, Laden, Artikel

# Register your models here.
admin.site.register(Einkaufsliste)
admin.site.register(Laden)
admin.site.register(Artikel)