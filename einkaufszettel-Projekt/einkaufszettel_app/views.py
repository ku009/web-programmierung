from django.shortcuts import redirect, render
from .models import Einkaufsliste, Laden, Artikel

# Create your views here.

# Einkaufslisten-Übersicht -> index.html
def index(request):
    aktive_listen = Einkaufsliste.objects.all()
    #summe = Einkaufsliste.summe
    context = {
        "einkaufslisten" : aktive_listen,
    #    "summe": summe,
    }
    return render(request, "einkaufszettel_app/index.html", context)

# Einkaufsliste hinzufügen -> index.html
def add_einkaufsliste(request):
    name = request.POST["name"]
    Einkaufsliste.objects.create(name=name)
    return redirect("index")

# Einkaufsliste löschen -> index.html --> /<int:einkaufsliste_id> in der URL übergeben
def delete_einkaufsliste(request, einkaufsliste_id):
    q = Einkaufsliste.objects.filter(pk=einkaufsliste_id)
    q.delete()
    return redirect("index")

# Artikel in anzeigen
def einkaufsliste_details(request, einkaufsliste_id):
    items = Artikel.objects.filter(einkaufsliste=einkaufsliste_id, status="False")
    # print("in", einkaufsliste_id, "Artikel: ", artikel)
    listen_name = Einkaufsliste.objects.get(pk=einkaufsliste_id)
    einheiten = Artikel.einheit_auswahl

    summe = 0
    for artikel in items:
        summe += artikel.preis

    Einkaufsliste.objects.filter(pk=einkaufsliste_id).update(summe = summe)
    
    laeden = Laden.objects.all()
    context = {
        "einkaufsliste_id": einkaufsliste_id,
        "laeden": laeden,
        "artikel": items,
        "summe": summe,
        "listen_name": listen_name,
        "einheiten": einheiten
    }
    return render(request, "einkaufszettel_app/details.html", context)

# Artikel hinzufügen
def add_artikel(request, einkaufsliste_id):
    name = request.POST["name"]
    anzahl = request.POST["anzahl"]
    einheit = request.POST["einheit"] #chocies?
    preis = request.POST["preis"]
    # id wird automatisch mit übergeben, je nachdem welche Liste gewählt wird
    einkaufsliste = Einkaufsliste.objects.get(pk=einkaufsliste_id)
    laden_id = request.POST["laden"]
    laden = Laden.objects.get(pk=laden_id)

    Artikel.objects.create(
        status = False,
        name = name,
        anzahl = anzahl,
        einheit = einheit,
        preis = preis,
        einkaufsliste = einkaufsliste,
        laden = laden,
    )
    return redirect("einkaufsliste_details", einkaufsliste_id)


# Artikel löschen
def delete_artikel(request, artikel_id, einkaufsliste_id):
    q = Artikel.objects.get(pk=artikel_id)
    q.delete()
    return redirect("einkaufsliste_details", einkaufsliste_id)


# Artikel wiederherstellen
def change_status_true(request, artikel_id, einkaufsliste_id):
    q = Artikel.objects.get(pk=artikel_id)
    q.status = True
    Artikel.objects.filter(pk=artikel_id).update(status = q.status)
    return redirect("einkaufsliste_details", einkaufsliste_id)

def change_status_false(request, artikel_id, einkaufsliste_id):
    q = Artikel.objects.get(pk=artikel_id)
    q.status = False
    Artikel.objects.filter(pk=artikel_id).update(status = q.status)
    return redirect("einkaufsliste_details", einkaufsliste_id)
   
def erledigte_artikel(request):
    gekaufte_artikel = Artikel.objects.filter(status="True")
    context = {
        "gekaufte_artikel": gekaufte_artikel,
    }
    return render(request, "einkaufszettel_app/archiv.html", context)
   
# Artikel bearbeiten




# Laden-Übersicht -> laeden.html
def laeden(request):
    laeden = Laden.objects.all()
    context = {
        "laeden": laeden,
    }
    return render(request, "einkaufszettel_app/laeden.html", context)

# Laden hinzufügen
def add_laden(request):
    name = request.POST["name"]
    Laden.objects.create(name=name)
    return redirect("laeden")

# Laden löschen -> id mit übergeben
def delete_laden(request, laden_id):
    q = Laden.objects.filter(pk=laden_id)
    q.delete()
    return redirect("laeden")
