from django.contrib import admin
from .models import Laden, Artikel

# Register your models here.
admin.site.register(Laden)
admin.site.register(Artikel)