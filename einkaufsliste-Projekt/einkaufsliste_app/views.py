from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.urls import reverse
from .models import Laden, Artikel

# Create your views here.

def index(request):
    #items = Artikel.objects.all() #Pluralform wählen!
    shops = Laden.objects.all()
    zu_kaufende_artikel = Artikel.objects.filter(status="False").order_by("laden")
    context = {
    #    "items": items,
        "shops": shops,
        "zu_kaufende_artikel": zu_kaufende_artikel,
    }

    return render(request, "einkaufsliste_app/einkaufsliste.html", context)

def new_item(request):
    status = False
    name = request.POST["name"]
    anzahl = request.POST["anzahl"]
    laden_id = request.POST["laden"]
    laden = Laden.objects.get(pk=laden_id)

    Artikel.objects.create(status = status,
                        name = name,
                        anzahl = anzahl,
                        laden = laden)
    return HttpResponseRedirect(reverse("index"))

def laeden(request):
    laeden = Laden.objects.all()
    context = {
        "laeden": laeden
    }

    return render(request, "einkaufsliste_app/laeden.html", context)

def new_laden(request):
    name = request.POST["shop"]
    Laden.objects.create(name = name)
    return HttpResponseRedirect(reverse("laeden"))

def delete_artikel(request, artikel_id):
    q = Artikel.objects.get(pk=artikel_id)
    q.delete()
    return redirect("index")

def delete_laden(request, laden_id):
    #wenn der Laden gelöscht wird, verschwinden auch die Artikel mit dem zu löschenden Laden
    q = Laden.objects.get(pk=laden_id)
    q.delete()
    return HttpResponseRedirect(reverse("laeden"))

def change_status_true(request, artikel_id):
    q = Artikel.objects.get(pk=artikel_id)
    q.status = True
    Artikel.objects.filter(pk=artikel_id).update(status = q.status)
    return redirect("index")

def change_status_false(request, artikel_id):
    q = Artikel.objects.get(pk=artikel_id)
    q.status = False
    Artikel.objects.filter(pk=artikel_id).update(status = q.status)
    return redirect("erledigte_artikel")
    
def erledigte_artikel(request):
    gekaufte_artikel = Artikel.objects.filter(status="True")
    context = {
        "gekaufte_artikel": gekaufte_artikel,
    }
    return render(request, "einkaufsliste_app/archiv.html", context)