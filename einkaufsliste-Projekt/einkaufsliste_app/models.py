from django.db import models

# Create your models here.
class Laden(models.Model):
    name = models.CharField(max_length=200, blank=False)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

class Artikel(models.Model):
    status = models.BooleanField()
    name = models.CharField(max_length=200, blank=False)
    anzahl = models.CharField(max_length=200, blank=False)
    laden = models.ForeignKey(to=Laden, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name