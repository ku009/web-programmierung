"""einkaufsliste URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('new_item/', views.new_item, name='new_item'),
    path('laeden/', views.laeden, name='laeden'),
    path('new_laden/', views.new_laden, name='new_laden'),
    path('delete_artikel/<int:artikel_id>/', views.delete_artikel, name='delete_artikel'),
    path('delete_laden/<int:laden_id>/', views.delete_laden, name='delete_laden'),
    path("erledigte_artikel/", views.erledigte_artikel, name="erledigte_artikel"),
    path("change_status_true/<int:artikel_id>/", views.change_status_true, name="change_status_true"),
    path("change_status_false/<int:artikel_id>/", views.change_status_false, name="change_status_false"),
]
