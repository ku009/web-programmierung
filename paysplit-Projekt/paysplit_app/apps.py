from django.apps import AppConfig


class PaysplitAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'paysplit_app'
