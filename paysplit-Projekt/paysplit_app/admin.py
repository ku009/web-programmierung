from django.contrib import admin
from .models import User, Payment, Payment_For_User

# Register your models here.
admin.site.register(User)
admin.site.register(Payment)
admin.site.register(Payment_For_User)