from django.shortcuts import HttpResponse, redirect, render
from .models import User, Payment, Payment_For_User
from django.db.models import Avg, Count, Min, Sum, Max
from . import models

# default bei edit setzen, bei dem pay_from
# pay_for in der edit einbauen


# Create your views here.
def index(request):
    # Alle Objekte in Payment
    payments = Payment.objects.all().order_by("datetime")
    
    summe = Payment.objects.aggregate(Sum("amount"))
    
    users = User.objects.all()
#    payments = Payment.objects.all()
#    overall_payment_amount = models.get_overall_payment_amount()
    who_is_next = models.who_is_next()


    context = {
        "users": users,
        "payments": payments,
        "summe": summe["amount__sum"],
#        "overall_payment_amount": overall_payment_amount,
        "who_is_next" : who_is_next,
    }
    return render(request, "paysplit_app/index.html", context)

# Leitet von index.html auf user_add.html
def user_add(request):
    return render(request, "paysplit_app/user_add.html")

# User anlegen
def func_create_user(request):
    name = request.POST["name"]
    User.objects.create(name=name)
    return redirect("index")

# Detailsansicht User
def user_details(request, user_id):
    user_id = user_id
    user_name = User.objects.get(pk=user_id)

    payments_from = Payment.objects.filter(from_user=user_id)
#    payments_for = Payment_For_User.objects.filter(user=user_id)
#    print(payments)

    context = {
        "user_id": user_id,
        "user_name": user_name,
        "payments": payments_from,
#        "payments_for": payments_for,
    }
    return render(request, "paysplit_app/user_details.html", context)

# Leitet von index.html auf payment_add.html
def payment_add(request):
    users = User.objects.all()
    context = {
        "users": users,
    }
    return render(request, "paysplit_app/payment_add.html", context)

# Zahlung erstellen
def func_payment_create(request):
    pay_from = request.POST["pay_from"]
    user_pay_from = User.objects.get(pk=pay_from)

    # User IDs
    user_id_list = []
    for user in request.POST.getlist("pay_for"):
        user_id_list.append(user)
    #print(user_list)

    # User aus DB holen
    user_list = []
    for user_id in user_id_list:
        user = User.objects.get(pk=user_id)
        user_list.append(user)

    amount = request.POST["amount"]
    amount = amount.replace(",", ".")

    description = request.POST["description"]
    payment = Payment.objects.create(from_user=user_pay_from, amount=amount, description=description)

    # Verknüpfen
    for user in user_list:
        Payment_For_User.objects.create(user=user, payment=payment)

    return redirect("index")

# Leitet von index.html auf payment_edit.html
def payment_edit(request, payment_id):
    q = Payment.objects.get(pk=payment_id)
    description = q.description
    created_at = q.datetime
    amount = q.amount
    pay_from = q.from_user
    pay_from_choices = User.objects.all()
    context = {
        "payment_id": payment_id,
        "pay_from_choices": pay_from_choices,
        "description": description,
        "created_at": created_at,
        "amount": amount,
        "pay_from": pay_from,
    }
    return render(request, "paysplit_app/payment_edit.html", context)

# Zahlungen bearbeiten
def func_payment_edit(request, payment_id):
    created_at = request.POST["datum"]
    description = request.POST["description"]
    amount = request.POST["amount"]
    amount = amount.replace(",", ".")
    pay_from = request.POST["pay_from"]
#    pay_for = request.POST["pay_for"]
    
    Payment.objects.filter(pk=payment_id).update(from_user=pay_from, 
                    amount=amount, description=description, datetime=created_at)
    return redirect("index")

# Leitet von index.html auf salden.html
def salden(request):
    users = User.objects.all()
    context = {
        "users": users,
    }
    return render(request, "paysplit_app/salden.html", context)
