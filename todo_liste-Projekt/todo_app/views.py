from django.shortcuts import render
from .models import Task
from django.http import HttpResponseRedirect
from django.urls import reverse

# Create your views here.

def index(request):
    tasks = Task.objects.order_by('task_deadline')
    context = {
        'tasks': tasks,
    }
    return render(request, 'todo_app/index.html', context)

def archiv(request):
    erledigte_tasks = Task.objects.filter(task_status__startswith="E")
    context = {
        "erledigte_tasks": erledigte_tasks,
    }
    return render(request, "todo_app/archiv.html", context)

def edit(request, task_id):
    q = Task.objects.get(pk=task_id)
    task_text_edit = q.task_text
    task_status_edit = q.task_status
    task_deadline_edit = q.task_deadline
    task_category_text_edit = q.task_category_text

    context = {
        "name": task_text_edit,
        "status": task_status_edit,
        "deadline": task_deadline_edit,
        "category": task_category_text_edit,
        "id": task_id
    }
    return render(request, "todo_app/edit.html", context)

def new_task(request):
    task_name = request.POST["name"]
    task_status = "O"
    task_deadline = request.POST["deadline"]
    task_category_text = request.POST["category"]

    print(task_name, task_status, task_deadline, task_category_text)

    Task.objects.create(task_text = task_name,
                        task_status = task_status,
                        task_deadline = task_deadline,
                        task_category_text = task_category_text)
    return HttpResponseRedirect(reverse('index'))

def edit(request, task_id):
    q = Task.objects.get(pk=task_id)
    task_text_edit = q.task_text
    task_status_edit = q.task_status
    task_deadline_edit = q.task_deadline
    task_category_text_edit = q.task_category_text

    context = {
        "name": task_text_edit,
        "status": task_status_edit,
        "deadline": task_deadline_edit,
        "category": task_category_text_edit,
        "id": task_id
    }
    
    return render(request, "todo_app/edit.html", context)

def change_task(request, task_id):
    q = Task.objects.get(pk=task_id)
    q.task_text = request.POST["name_edit"]
    q.task_status = request.POST["status_edit"]
    q.task_deadline = request.POST["deadline_edit"]
    q.task_category_text = request.POST["category_edit"]

    Task.objects.filter(pk=task_id).update(task_text = q.task_text,
                        task_status = q.task_status,
                        task_deadline = q.task_deadline,
                        task_category_text = q.task_category_text)
    return HttpResponseRedirect(reverse('index'))

def delete_task(request, task_id):
    q = Task.objects.get(pk=task_id)
    q.delete()
    return HttpResponseRedirect(reverse('index'))
