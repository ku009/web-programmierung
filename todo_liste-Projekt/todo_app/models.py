from django.db import models
from datetime import datetime


# Create your models here.

class Task(models.Model):
    task_text = models.CharField(max_length=200)
    task_status_auswahl = [
        ("O", "offen"),
        ("E", "erledigt"),
        ("L", "Löschen")
    ]
    task_status = models.CharField(max_length=1, choices=task_status_auswahl, default="O")
    task_deadline = models.DateField(default="9999-12-31")
    task_category_text = models.CharField(max_length=200, default="Uni")

    def __str__(self):
        return f"{self.task_text} {self.task_status} {self.task_deadline} {self.task_category_text}"
