"""todo_liste URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('archiv/', views.archiv, name='archiv'),
    path('new_task/', views.new_task, name='new_task'),
    # ex: /todo_app/5/edit/
    path('<int:task_id>/edit/', views.edit, name='edit'),
    path('<int:task_id>/change_task/', views.change_task, name='change_task'),
    path('<int:task_id>/delete_task/', views.delete_task, name='delete_task')
]
