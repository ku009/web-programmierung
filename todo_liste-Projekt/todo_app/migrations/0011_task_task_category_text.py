# Generated by Django 4.1 on 2022-10-23 14:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('todo_app', '0010_alter_task_task_deadline'),
    ]

    operations = [
        migrations.AddField(
            model_name='task',
            name='task_category_text',
            field=models.CharField(default='Uni', max_length=200),
        ),
    ]
