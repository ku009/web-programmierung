# Generated by Django 4.1 on 2022-10-23 10:47

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('todo_app', '0004_alter_task_task_deadline'),
    ]

    operations = [
        migrations.AlterField(
            model_name='task',
            name='task_deadline',
            field=models.DateTimeField(default=datetime.datetime(2022, 10, 23, 10, 47, 57, 631938, tzinfo=datetime.timezone.utc)),
        ),
    ]
