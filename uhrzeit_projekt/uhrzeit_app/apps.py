from django.apps import AppConfig


class UhrzeitAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'uhrzeit_app'
