from django.shortcuts import render
import datetime
import time

# Create your views here.
def uhrzeit_app(request):
    uhrzeit = time.localtime()
    uhrzeit = time.strftime("%H:%M", uhrzeit)
    datum = datetime.date.today()
    context = {
        "uhrzeit": uhrzeit, "datum": datum,
    }
    return render(request, 'uhrzeit_app/uhrzeit_app.html', context)